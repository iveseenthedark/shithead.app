import * as functions from "firebase-functions";
import * as _ from "lodash";

import * as cors from "cors";
import * as express from "express";

import Game from "./game/Game";
import Player from "./game/Player";

import { TypedJSON } from "typedjson";

const GAME_PATH = "Games";
let is_firebase_initialized = false;

///////////////////////
// UTILITY METHODS
///////////////////////
const useFirebase = async () => {
  const admin = await import("firebase-admin");
  if (!is_firebase_initialized) {
    admin.initializeApp();
    is_firebase_initialized = true;
  }
  return admin;
};

const validate = (request: express.Request, keys: string[]) => {
  return keys.every((key) => _.has(request.query, key));
};

const save = async (game: Game) => {
  const admin = await useFirebase();
  await admin
    .firestore()
    .collection(GAME_PATH)
    .doc(game.id)
    .set(JSON.parse(JSON.stringify(game)));
};

const load = async (id: string): Promise<Game> => {
  const admin = await useFirebase();
  const gameData = await admin.firestore().collection(GAME_PATH).doc(id).get();

  const serializer = new TypedJSON(Game);
  const game = serializer.parse(gameData.data());

  if (game === undefined) {
    throw new Error("Error parsing game data");
  }
  return game;
};

///////////////////////
///////////////////////

///////////////////////
// Express Configuration
///////////////////////
const app = express();
app.use(cors({ origin: true }));

///////////////////////
// Express Methods
///////////////////////

app.get("/create_game", async (request, response) => {
  const game = new Game();
  game.log("Game created");
  await save(game);
  response.send({ id: game.id });
});

app.get("/join_game", async (request, response) => {
  if (!validate(request, ["game_id", "name"])) {
    response.status(400);
    response.send({ error: "Request must specify game id and player name" });
    return;
  }

  const { game_id, name } = request.query;
  const game = await load(<string>game_id);

  const player = new Player(name.toString());
  game.join(player);

  game.log(`Player ${name} joined`);

  await save(game);
  response.send({ id: player.id });
});

app.get("/start_game", async (request, response) => {
  if (!validate(request, ["game_id"])) {
    response.status(400);
    response.send({ error: "Request must specify game id and player name" });
    return;
  }

  const { game_id } = request.query;
  const game = await load(<string>game_id);

  if (game.isStarted()) {
    response.status(400);
    response.send({ error: "Game has already started" });
    return;
  }

  game.start();
  game.deal();

  game.log(`Game started`);

  await save(game);
  response.send(game);
});

app.get("/play_card", async (request, response) => {
  if (!validate(request, ["game_id", "player_id", "card_code", "hand"])) {
    response.status(400);
    response.send({ error: "Request must specify game id and player name" });
    return;
  }

  const { game_id, player_id, card_code, hand } = request.query;
  const game = await load(<string>game_id);

  if (!game.isStarted()) {
    response.status(400);
    response.send({ error: "Game hasn't started" });
    return;
  }

  const player = game.getPlayer(<string>player_id);

  const pile =
    hand === "InHand"
      ? player.inHandHand
      : hand === "FaceUp"
      ? player.faceUpHand
      : player.faceDownHand;

  const card = pile.getCard(<string>card_code);
  if (_.isNil(card)) {
    response.status(400);
    response.send({ error: "Card isn't in hand" });
    return;
  }

  pile.pullCard(card);
  game.playedPile.addCard(card);

  if (!game.deck.isEmpty() && player.inHandHand.cards.length < 3) {
    const newCard = game.deck.pullTop();
    player.inHandHand.addCard(newCard);
  }

  game.log(`${player.name} played ${card.toString()}`);
  game.lastPlayed = player;

  await save(game);
  response.send(game);
});

app.get("/swap", async (request, response) => {
  if (!validate(request, ["game_id", "player_id", "in_hand", "out_hand"])) {
    response.status(400);
    response.send({ error: "Request must specify game id and player name" });
    return;
  }

  const { game_id, player_id, in_hand, out_hand } = request.query;
  const game = await load(<string>game_id);

  if (!game.isStarted()) {
    response.status(400);
    response.send({ error: "Game hasn't started" });
    return;
  }

  const player = game.getPlayer(<string>player_id);
  const inHandCard = player.inHandHand.getCard(<string>in_hand);
  const faceUpCard = player.faceUpHand.getCard(<string>out_hand);
  if (_.isNil(inHandCard) || _.isNil(faceUpCard)) {
    response.status(400);
    response.send({ error: "Card isn't in hands" });
    return;
  }

  player.inHandHand.pullCard(inHandCard);
  player.faceUpHand.pullCard(faceUpCard);

  player.inHandHand.addCard(faceUpCard);
  player.faceUpHand.addCard(inHandCard);

  await save(game);
  response.send(game);
});

app.get("/pick_up", async (request, response) => {
  if (!validate(request, ["game_id", "player_id"])) {
    response.status(400);
    response.send({ error: "Request must specify game id and player name" });
    return;
  }

  const { game_id, player_id } = request.query;
  const game = await load(<string>game_id);

  if (!game.isStarted()) {
    response.status(400);
    response.send({ error: "Game hasn't started" });
    return;
  }

  const player = game.getPlayer(<string>player_id);
  game.playedPile.cards.forEach((card) => {
    player.inHandHand.addCard(card);
  });
  game.playedPile.cards.splice(0, game.playedPile.cards.length);

  game.log(`${player.name} picked up the pile`);

  await save(game);
  response.send(game);
});

app.get("/pick_up_top", async (request, response) => {
  if (!validate(request, ["game_id", "player_id"])) {
    response.status(400);
    response.send({ error: "Request must specify game id and player name" });
    return;
  }

  const { game_id, player_id } = request.query;
  const game = await load(<string>game_id);

  if (!game.isStarted()) {
    response.status(400);
    response.send({ error: "Game hasn't started" });
    return;
  }

  const player = game.getPlayer(<string>player_id);
  const lastCard = game.playedPile.cards.shift();
  if (lastCard) {
    player.inHandHand.addCard(lastCard);
  }

  await save(game);
  response.send(game);
});

app.get("/burn", async (request, response) => {
  if (!validate(request, ["game_id"])) {
    response.status(400);
    response.send({ error: "Request must specify game id and player name" });
    return;
  }

  const { game_id } = request.query;
  const game = await load(<string>game_id);

  if (!game.isStarted()) {
    response.status(400);
    response.send({ error: "Game hasn't started" });
    return;
  }

  game.playedPile.cards.forEach((card) => {
    game.discardPile.addCard(card);
  });
  game.playedPile.cards.splice(0, game.playedPile.cards.length);

  game.log(`Pile was burned`);

  await save(game);
  response.send(game);
});

///////////////////////
///////////////////////
exports.api = functions.https.onRequest(app);
