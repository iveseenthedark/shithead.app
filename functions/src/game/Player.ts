import { jsonObject, jsonMember } from "typedjson";
import Pile from "../pile/Pile";
import Utils from "../utils/Utils";

@jsonObject()
class Player {
  @jsonMember({ constructor: String })
  public readonly id: string;
  @jsonMember({ constructor: String })
  public readonly name: string;
  @jsonMember({ constructor: Boolean })
  public dealer: boolean;

  @jsonMember({ constructor: Pile })
  public readonly inHandHand: Pile;
  @jsonMember({ constructor: Pile })
  public readonly faceUpHand: Pile;
  @jsonMember({ constructor: Pile })
  public readonly faceDownHand: Pile;

  constructor(name: string) {
    this.name = name;
    this.id = Utils.id();

    this.inHandHand = new Pile();
    this.faceUpHand = new Pile();
    this.faceDownHand = new Pile();

    this.dealer = false;
  }
}

export default Player;
