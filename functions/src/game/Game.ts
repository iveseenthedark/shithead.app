import { jsonArrayMember, jsonMember, jsonObject } from "typedjson";

import Player from "./Player";
import Deck from "../deck/Deck";
import Pile from "../pile/Pile";
import Utils from "../utils/Utils";

type LogItem = {
  timestamp: number;
  msg: string;
};

@jsonObject()
class Game {
  @jsonMember({ constructor: Boolean })
  private started: boolean;
  @jsonMember({ constructor: String })
  public readonly id: string;

  @jsonMember({ constructor: Pile })
  public readonly deck: Pile;
  @jsonMember({ constructor: Pile })
  public readonly playedPile: Pile;
  @jsonMember({ constructor: Pile })
  public readonly discardPile: Pile;

  @jsonArrayMember(Player)
  private readonly players: Player[];

  @jsonArrayMember(Object)
  private readonly logger: LogItem[];

  @jsonMember({ constructor: Player })
  public lastPlayed: Player | undefined;

  constructor() {
    this.id = Utils.gameId();
    this.started = false;

    this.players = new Array();
    this.logger = new Array();

    this.deck = new Pile();
    this.playedPile = new Pile();
    this.discardPile = new Pile();
  }

  public join(player: Player): void {
    if (this.isStarted()) {
      throw new Error("Game has already started");
    }
    if (this.players.length === 0) {
      player.dealer = true;
    }
    this.players.push(player);
  }

  public deal(): void {
    if (!this.isStarted()) {
      throw new Error("Game hasn't started yet!");
    }

    const cards = new Deck();
    cards.shuffle();

    // Deal player hands
    this.players.forEach((player) => {
      player.inHandHand.addCard(cards.pop());
      player.inHandHand.addCard(cards.pop());
      player.inHandHand.addCard(cards.pop());

      player.faceDownHand.addCard(cards.pop());
      player.faceDownHand.addCard(cards.pop());
      player.faceDownHand.addCard(cards.pop());

      player.faceUpHand.addCard(cards.pop());
      player.faceUpHand.addCard(cards.pop());
      player.faceUpHand.addCard(cards.pop());
    });

    // Reset of cards go into deck pile
    while (!cards.isEmpty()) {
      this.deck.addCard(cards.pop());
    }
  }

  public start(): void {
    if (this.isStarted()) {
      throw new Error("Game has already started!");
    }
    if (this.players.length < 2) {
      throw new Error("Need to friend to play with loser!");
    }
    this.started = true;
  }

  public isStarted(): boolean {
    return this.started;
  }

  public getPlayer(id: string): Player {
    const player = this.players.find((p) => p.id === id);
    if (!player) {
      throw new Error("Player not found");
    }
    return player;
  }

  public log(msg: string): void {
    this.logger.push({ timestamp: new Date().getTime(), msg });
  }
}

export default Game;
