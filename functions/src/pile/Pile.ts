import * as _ from "lodash";
import { jsonArrayMember, jsonObject } from "typedjson";
import { Card } from "../deck/Card";

@jsonObject()
class Pile {
  @jsonArrayMember(Card)
  public readonly cards: Card[] = new Array();

  public addCard(card: Card) {
    this.cards.unshift(card);
  }

  public pullCard(card: Card): void {
    if (_.find(this.cards, card) === undefined) {
      throw new Error(`Card ${card} if not in hand`);
    }
    _.pull(this.cards, card);
  }

  public pullTop(): Card {
    if (this.isEmpty()) {
      throw new Error(`No cards left to take!`);
    }
    return <Card>this.cards.pop();
  }

  public isEmpty(): boolean {
    return this.cards.length === 0;
  }

  public getCard(code: string): Card | undefined {
    return _.find(this.cards, (card: Card) => card.code() === code);
  }
}

export default Pile;
