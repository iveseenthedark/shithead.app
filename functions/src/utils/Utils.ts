import { customAlphabet, urlAlphabet } from "nanoid";

export default class Utils {
  private static readonly idGenerator = customAlphabet(urlAlphabet, 21);
  private static readonly codeGenerator = customAlphabet("0123456789ABCDEF", 5);

  /**
   * Generate random URL-safe ids
   */
  static id(): string {
    return Utils.idGenerator();
  }

  /**
   * Generate random URL-safe ids
   */
  static gameId(): string {
    return Utils.codeGenerator();
  }
}
