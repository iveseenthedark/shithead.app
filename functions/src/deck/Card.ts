import { jsonMember, jsonObject } from "typedjson";

enum Suit {
  Spade = "S",
  Club = "C",
  Heart = "H",
  Diamond = "D",
}

@jsonObject()
class Card {
  @jsonMember({ constructor: String })
  public readonly suit: Suit;
  @jsonMember({ constructor: Number })
  public readonly value: number;

  constructor(suit: Suit, value: number) {
    this.suit = suit;
    this.value = value;
  }

  /**
   * Get the human readable version of the card value
   */
  private getCardName(): string {
    switch (this.value) {
      case 1:
        return "Ace";
      case 11:
        return "Jack";
      case 12:
        return "Queen";
      case 13:
        return "King";
      default:
        return this.value.toString();
    }
  }

  public code(): string {
    return `${this.value}${this.suit}`;
  }

  public toString(): string {
    return `${this.suit} ${this.getCardName()}`;
  }
}

export { Card, Suit };
