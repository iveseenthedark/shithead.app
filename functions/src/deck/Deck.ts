import * as _ from "lodash";
import { jsonArrayMember, jsonObject } from "typedjson";
import { Card, Suit } from "./Card";

@jsonObject
class Deck {
  @jsonArrayMember(Card)
  private cards: Card[];

  constructor() {
    // build deck
    this.cards = new Array();
    for (let i = 1; i <= 13; i++) {
      this.cards.push(new Card(Suit.Heart, i));
      this.cards.push(new Card(Suit.Diamond, i));
      this.cards.push(new Card(Suit.Club, i));
      this.cards.push(new Card(Suit.Spade, i));
    }
  }

  public shuffle() {
    this.cards = _.shuffle(this.cards);
  }

  public pop(): Card {
    if (this.isEmpty()) {
      throw new Error("No cards left in deck");
    }
    return <Card>this.cards.pop();
  }

  public isEmpty(): boolean {
    return this.cards.length === 0;
  }

  public toString() {
    return this.cards.map((card) => card.toString()).join("\n");
  }
}

export default Deck;
