// const URL_BASE = `http://${window.location.hostname}:5000/online-shithead/us-central1/api`;
const URL_BASE = `https://us-central1-online-shithead.cloudfunctions.net/api`;

const toQuery = (values) => {
  return Object.entries(values)
    .map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
    .join("&");
};

const API = {
  createGame: () => {
    return fetch(`${URL_BASE}/create_game`).then((rsp) => rsp.json());
  },

  joinGame: (game_id, name) => {
    const params = toQuery({ game_id, name });
    return fetch(`${URL_BASE}/join_game?${params}`).then((rsp) => rsp.json());
  },

  startGame: (game_id) => {
    const params = toQuery({ game_id });
    return fetch(`${URL_BASE}/start_game?${params}`).then((rsp) => rsp.json());
  },

  playCard: (game_id, player_id, card_code, hand) => {
    const params = toQuery({ game_id, player_id, card_code, hand });
    return fetch(`${URL_BASE}/play_card?${params}`).then((rsp) => rsp.json());
  },

  swapCard: (game_id, player_id, in_hand, out_hand) => {
    const params = toQuery({ game_id, player_id, in_hand, out_hand });
    return fetch(`${URL_BASE}/swap?${params}`).then((rsp) => rsp.json());
  },

  pickUp: (game_id, player_id) => {
    const params = toQuery({ game_id, player_id });
    return fetch(`${URL_BASE}/pick_up?${params}`).then((rsp) => rsp.json());
  },

  pickUpLast: (game_id, player_id) => {
    const params = toQuery({ game_id, player_id });
    return fetch(`${URL_BASE}/pick_up_top?${params}`).then((rsp) => rsp.json());
  },

  burn: (game_id, player_id) => {
    const params = toQuery({ game_id, player_id });
    return fetch(`${URL_BASE}/burn?${params}`).then((rsp) => rsp.json());
  },
};

export default API;
