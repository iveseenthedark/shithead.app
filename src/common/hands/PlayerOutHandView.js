import React from "react";
import { Heading } from "@chakra-ui/core";
import { FaceUpHand, Grid, HandContainer, OutOfHandHolder } from "../Grid";
import { CardTypes } from "../../utils/utils";
import { PlayableCard } from "../cards/PlayableCard";

export const PlayerOutHandView = ({ player, onSwapCard, showName = true }) => {
  return (
    <HandContainer>
      {showName && (
        <Heading as="h3" size="lg" marginTop={-5} color="orange.200">
          {`${player.name} (${player.inHandHand.cards.length} cards)`}
        </Heading>
      )}
      <OutOfHandHolder>
        {!!player.faceUpHand.cards.length && (
          <FaceUpHand>
            {player.faceUpHand.cards.map((card) => (
              <PlayableCard
                key={`${card.value}${card.suit}`}
                card={card}
                type={CardTypes.FACE_UP_CARD}
                accept={CardTypes.IN_HAND_CARD}
                onDrop={(in_hand) => onSwapCard(in_hand.card, card)}
              />
            ))}
          </FaceUpHand>
        )}

        <Grid>
          {player.faceDownHand.cards.map((card) => (
            <PlayableCard
              key={`${card.value}${card.suit}`}
              card={card}
              type={CardTypes.FACE_DN_CARD}
              flipped
            />
          ))}
        </Grid>
      </OutOfHandHolder>
    </HandContainer>
  );
};
