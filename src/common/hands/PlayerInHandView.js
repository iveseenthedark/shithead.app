import React from "react";
import {Grid, HandContainer} from "../Grid";
import { Box } from "@chakra-ui/core";
import { CardTypes } from "../../utils/utils";
import {PlayableCard} from "../cards/PlayableCard";

export const PlayerInHandView = ({ hand, onSwapCard }) => {
  return (
    <HandContainer>
      <Grid>
        {hand.cards.map((card) =>
          card === undefined ? (
            <Box key={"blank"}></Box>
          ) : (
            <PlayableCard
              key={`${card.value}${card.suit}`}
              card={card}
              size="5rem"
              type={CardTypes.IN_HAND_CARD}
              accept={CardTypes.FACE_UP_CARD}
              onDrop={(out_hand) => onSwapCard(card, out_hand.card)}
            />
          )
        )}
      </Grid>
    </HandContainer>
  );
};
