import styled from "@emotion/styled";
import { Box } from "@chakra-ui/core";

export const Grid = styled(Box)`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 0.5rem;
`;

export const CardContainer = styled(Box)`
  border-radius: 7px;
  border-style: solid;
  border-width: 3px;
  border-color: ${({ isHover }) => (isHover ? "tomato" : "transparent")};
`;

export const HandContainer = styled(Box)`
  padding: 1.25rem;
  border: 1px solid;
  border-radius: 0.5rem;
  background-color: rgba(1, 1, 1, 0.25);
  box-shadow: 0px 0px 20px 5px inset rgba(1, 1, 1, 0.25);
`;

export const PlayZone = styled(Box)`
  position: relative;
  text-align: center;
  margin: ${({ theme }) => theme.space[3]};
  padding: ${({ theme }) => theme.space[5]};
  width: 19.5rem;
  
  border-radius: ${({ theme }) => theme.radii.lg};
  border: 5px dashed ${({ theme, isHover }) =>
    isHover ? "tomato" : theme.colors.gray[500]}};
`;

export const Table = styled(Box)`
  position: relative;
  padding: ${({ theme }) => theme.space[10]};
  width: 50rem;
  height: 50rem;
`;

export const PlayerHand = styled(Box)`
  position: absolute;
  left: 50%;
  transform: ${({ idx, count }) => {
    return `translateX(-50%) rotate(${Math.floor(360 / count) * idx}deg)`;
  }};
  transform-origin: ${({ theme }) => {
    return `50% calc(25rem - ${theme.space[10]})`;
  }};
  h3 {
    transform: ${({ idx, count }) => {
      const angle = Math.floor(360 / count) * idx;
      return `rotate(${angle > 90 && angle < 270 ? 180 : 0}deg)`;
    }};
  }
`;

export const DeckContainer = styled(Box)`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

export const OutOfHandHolder = styled(Box)`
  position: relative;
`;

export const FaceUpHand = styled(Grid)`
  position: absolute;
  top: 5px;
  left: 5px;
  width: 100%;
  height: 100%;
`;
