import React from "react";
import { Box, Spinner } from "@chakra-ui/core";
import styled from "@emotion/styled";

const Mask = styled(Box)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default () => (
  <Mask>
    <Spinner size="xl" thickness="4px" color="blue.500" />
  </Mask>
);
