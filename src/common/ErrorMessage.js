import React from "react";
import { Flex, Heading, Icon } from "@chakra-ui/core";

export default function ErrorMessage({ componentStack, error }) {
  return (
    <Flex size="100%" align="center" justify="center" direction="column">
      <Icon name="warning" size="250px" color="red.500" padding={10} />
      <Heading>{error.toString()}</Heading>
    </Flex>
  );
}
