import { Avatar, Box, Text } from "@chakra-ui/core";
import React from "react";

export function PlayerAvatar({ player, size = "2xl", showName = false }) {
  return (
    <Box textAlign="center">
      <Avatar
        size={size}
        name={player.name}
        src={`https://api.adorable.io/avatars/128/${player.name}`}
      />
      {showName && (
        <Text textAlign="center" fontSize={size}>
          {player.name}
        </Text>
      )}
    </Box>
  );
}
