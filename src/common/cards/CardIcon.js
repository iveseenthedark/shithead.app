import React from "react";
import { Box, Text } from "@chakra-ui/core";

const SuitIcons = {
  S: "♠",
  C: "♣",
  H: "♥",
  D: "♦",
};

export default function CardIcon({ card: { value, suit } }) {
  const colour = suit === "H" || suit === "D" ? "red.600" : "black";
  const toCardKey = (value) => {
    switch (value) {
      case 1:
        return "A";
      case 11:
        return "J";
      case 12:
        return "Q";
      case 13:
        return "K";
      default:
        return value;
    }
  };

  return (
    <Box color={colour} mx={1}>
      <Text fontSize="xs">{`${toCardKey(value)}${SuitIcons[suit]}`}</Text>
    </Box>
  );
}
