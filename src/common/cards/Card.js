import React from "react";
import { Image } from "@chakra-ui/core";
import utils from "../../utils/utils";

const PlayingCards = {};
const suits = ["S", "C", "D", "H"];

for (let i = 1; i <= 13; i++) {
  suits.forEach((suit) => {
    const key = `${i}${suit}`;
    PlayingCards[key] = require(`../../assets/cards/${key}.svg`);
  });
}
PlayingCards.flipped = require(`../../assets/cards/BACK_BLUE.svg`);

export const Card = ({ size, card, flipped = false }) => {
  const code = flipped ? "Hidden Card" : utils.cardCode(card);
  const src = flipped ? PlayingCards.flipped : PlayingCards[code];

  if (src === undefined) {
    throw new Error(`Playing card ${code} isn't valid`);
  }

  return <Image width={size} src={src} alt={code} data-card={code} />;
};
