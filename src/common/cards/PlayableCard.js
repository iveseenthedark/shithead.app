import React from "react";
import { useDrag, useDrop } from "react-dnd";
import { CardContainer } from "../Grid";
import { Card } from "./Card";

const PlayableCard = ({
  card,
  type,
  onDrop,
  accept = "",
  size = "5rem",
  flipped = false,
}) => {
  // let this card be dragged
  const [{ isDragging }, drag] = useDrag({
    item: { type, card },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  // Allow cards to be dropped on this
  const [{ isHover }, drop] = useDrop({
    accept,
    drop: (item) => {
      onDrop(item);
    },
    collect: (monitor) => ({
      isHover: monitor.isOver(),
    }),
  });

  return (
    <CardContainer
      ref={(node) => drag(drop(node))}
      isDragging={isDragging}
      isHover={isHover}
    >
      <Card size={size} card={card} flipped={flipped} />
    </CardContainer>
  );
};
export { PlayableCard };
