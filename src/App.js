import React, { Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { withTracker } from "./utils/Analytics";

import ErrorBoundary from "react-error-boundary";
import ErrorMessage from "./common/ErrorMessage";

import { FirebaseAppProvider } from "reactfire";
import { firebase as config } from "./config";

import { Box, Flex, Link, Spinner, Text } from "@chakra-ui/core";

import CreateGame from "./routes/CreateGame";
import DisplayGame from "./routes/DisplayGame";
import JoinGame from "./routes/JoinGame";
import PlayGame from "./routes/PlayGame";
import styled from "@emotion/styled";

import { HTML5Backend } from "react-dnd-html5-backend";
import { DndProvider } from "react-dnd";
import { TouchBackend } from "react-dnd-touch-backend";
import { usePreview } from "react-dnd-preview";
import { Card } from "./common/cards/Card";
import { CardTypes } from "./utils/utils";

const Footer = styled.div`
  position: fixed;
  bottom: 0;
  right: 0;
`;

const MyPreview = () => {
  const { display, itemType, item, style } = usePreview();
  if (!display) {
    return null;
  }
  return (
    <Box style={{ ...style, zIndex: 99999 }}>
      <Card
        card={item.card}
        size="5rem"
        flipped={item.type === CardTypes.FACE_DN_CARD}
      />
    </Box>
  );
};

export default function App() {
  return (
    <ErrorBoundary FallbackComponent={ErrorMessage}>
      <FirebaseAppProvider firebaseConfig={config}>
        <DndProvider backend={TouchBackend}>
          <MyPreview />
          <Flex
            size="100%"
            align="center"
            justify="center"
            direction="column"
            bg="green.600"
            overflowY="scroll"
          >
            <Suspense fallback={<Spinner size="xl" />}>
              <Router>
                <Switch>
                  <Route path="/" exact component={withTracker(CreateGame)} />
                  <Route
                    path="/display/:id"
                    component={withTracker(DisplayGame)}
                  />
                  <Route path="/join" component={withTracker(JoinGame)} />
                  <Route
                    path="/play/:game/:player"
                    component={withTracker(PlayGame)}
                  />
                </Switch>
              </Router>
            </Suspense>
            <Footer>
              <Text as="strong" color="pink.500">
                <Link href="https://iveseenthedark.bitbucket.io/">
                  iveseenthedark
                </Link>
              </Text>
            </Footer>
          </Flex>
        </DndProvider>
      </FirebaseAppProvider>
    </ErrorBoundary>
  );
}
