import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { Button, Heading, Spinner, Stack, Text } from "@chakra-ui/core";
import API from "../api";

export default function CreateGame() {
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const doCreate = () => {
    setLoading(true);
    API.createGame().then(({ id }) => {
      history.push(`/display/${id}`);
    });
  };

  return (
    <>
      <Heading textAlign="center" fontSize="150pt">
        <span role="img" aria-label="Shithead Logo">
          💩
        </span>
      </Heading>
      <Heading fontSize="3xl" textAlign="center">
        Shithead Online
      </Heading>
      <Text fontSize="xl">Socially responsible gaming.</Text>
      <Stack padding={6}>
        <Button onClick={doCreate} variantColor="yellow" width={200}>
          {loading ? <Spinner /> : "Create Game"}
        </Button>
        <Button as="a" href="/join" variantColor="pink" width={200}>
          Join Game
        </Button>
      </Stack>
    </>
  );
}
