import React, { useState } from "react";
import API from "../api";
import utils, { CardTypes } from "../utils/utils";
import { Box, Button, ButtonGroup, Flex, Text } from "@chakra-ui/core";
import Loading from "../common/Loading";
import { PlayerOutHandView } from "../common/hands/PlayerOutHandView";
import { PlayerInHandView } from "../common/hands/PlayerInHandView";
import { useDrop } from "react-dnd";
import { PlayZone } from "../common/Grid";

const PlayCardDropZone = ({ onPlayCard }) => {
  const [{ isHover }, drop] = useDrop({
    accept: [
      CardTypes.IN_HAND_CARD,
      CardTypes.FACE_UP_CARD,
      CardTypes.FACE_DN_CARD,
    ],
    drop: (item) => {
      onPlayCard(utils.cardCode(item.card), item.type);
    },
    collect: (monitor) => ({
      isHover: monitor.isOver(),
    }),
  });

  return (
    <PlayZone ref={drop} isHover={isHover}>
      <Text fontSize="2xl" color="gray.700">
        Play Card
      </Text>
    </PlayZone>
  );
};

export const Play = ({ game, player }) => {
  const [loading, setLoading] = useState(false);

  const onPlayCard = (card, hand) => {
    setLoading(true);
    API.playCard(game.id, player.id, card, hand).then(() => {
      setLoading(false);
    });
  };

  const onSwapCard = (in_hand, out_hand) => {
    setLoading(true);
    API.swapCard(
      game.id,
      player.id,
      utils.cardCode(in_hand),
      utils.cardCode(out_hand)
    ).then(() => {
      setLoading(false);
    });
  };

  const onPickUp = () => {
    setLoading(true);
    API.pickUp(game.id, player.id).then(() => {
      setLoading(false);
    });
  };

  const onPickUpLast = () => {
    setLoading(true);
    API.pickUpLast(game.id, player.id).then(() => {
      setLoading(false);
    });
  };

  const onBurn = () => {
    setLoading(true);
    API.burn(game.id, player.id).then(() => {
      setLoading(false);
    });
  };

  return (
    <>
      {loading && <Loading />}
      <Flex height="100%" width="100%" direction={"column"} alignItems="center">
        <PlayCardDropZone onPlayCard={onPlayCard} />

        <Box maxWidth={400} p={3} flexGrow={1}>
          {player.inHandHand.cards.length > 0 && (
            <PlayerInHandView
              hand={player.inHandHand}
              columns={4}
              onSwapCard={onSwapCard}
            />
          )}
        </Box>

        <Box maxWidth={400} p={3}>
          <PlayerOutHandView
            player={player}
            showName={false}
            onSwapCard={onSwapCard}
          />
        </Box>

        <ButtonGroup p="5px" width="100%" bg="gray.900">
          <Button onClick={onBurn}>Burn</Button>
          <Button onClick={onPickUp}>Pick Up</Button>
          <Button onClick={onPickUpLast}>Undo Last</Button>
        </ButtonGroup>
      </Flex>
    </>
  );
};
