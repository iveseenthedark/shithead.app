import React, { useState } from "react";
import * as _ from "lodash";
import { Box, Code, Heading, SimpleGrid, Stack, Text } from "@chakra-ui/core";
import { useFirestore, useFirestoreDocData } from "reactfire";
import { PlayerAvatar } from "../common/PlayerAvatar";
import { Card } from "../common/cards/Card";
import Poop from "../assets/emojipoo.svg";
import CardIcon from "../common/cards/CardIcon";
import { PlayerOutHandView } from "../common/hands/PlayerOutHandView";
import { CardPlaceholder } from "./CardPlaceholder";
import { DeckContainer, PlayerHand, Table } from "../common/Grid";
import utils from "../utils/utils";

const img = new Image();
img.src = Poop;

const PlayersJoining = ({ game }) => {
  return (
    <>
      <Heading textAlign="center">Join the Game From Your Phone!</Heading>
      <Text fontSize="xl">
        Visit <Code>https://shithead.app/join</Code> and enter the code below
      </Text>
      <Box padding={6}>
        <Code variantColor="gray" padding={5} fontSize={30}>
          {game.id}
        </Code>
      </Box>
      <SimpleGrid columns={[2, null, 3]} spacing="40px">
        {game.players.map((player) => (
          <PlayerAvatar key={player.id} player={player} showName />
        ))}
      </SimpleGrid>
    </>
  );
};

const DeckView = ({ game }) => {
  return (
    <SimpleGrid columns={2} spacing={2} alignItems="center">
      {game.deck.cards.length === 0 ? (
        <CardPlaceholder />
      ) : (
        <Card size="10rem" flipped />
      )}
      {game.playedPile.cards.length === 0 ? (
        <CardPlaceholder />
      ) : (
        <Card size="10rem" card={game.playedPile.cards[0]} />
      )}
      <Text
        as="strong"
        fontSize="xs"
      >{`Cards remaining: ${game.deck.cards.length}`}</Text>
      <Stack isInline>
        {_.slice(game.playedPile.cards, 1, 6).map((card) => (
          <CardIcon key={utils.cardCode(card)} card={card} />
        ))}
      </Stack>
    </SimpleGrid>
  );
};

const Log = ({ game }) => (
  <>
    <Heading size="sm">Moves</Heading>
    <Text as="pre" fontFamily="monospace" height="20rem" overflowY="scroll">
      {game.logger.reverse().map((entry, idx) => `${entry.msg}\n`)}
    </Text>
  </>
);

const GroupGameView = ({ game }) => {
  const lastPlayedIndex = game.players.findIndex(
    (p) => p.id === game.lastPlayed.id
  );
  const nextPlayerIndex = (lastPlayedIndex + 1) % game.players.length;

  return (
    <>
      <Box
        position="absolute"
        top={5}
        left={5}
        bg="gray.200"
        p={5}
        boxShadow="5px 5px 0px 0px black"
      >
        <Log game={game} />
      </Box>
      <Table p={5}>
        <DeckContainer>
          <DeckView game={game} />
        </DeckContainer>
        {game.players.map((player, idx) => (
          <PlayerHand key={player.id} idx={idx} count={game.players.length}>
            <Box
              bg={"orange.200"}
              w="75%"
              p={1}
              roundedTop={2}
              m={"0 auto"}
              textAlign="center"
              fontSize={"xs"}
              opacity={idx === nextPlayerIndex ? 1 : 0}
            >
              Your Move!!
            </Box>
            <PlayerOutHandView player={player} showAvatar />
          </PlayerHand>
        ))}
      </Table>
    </>
  );
};

export default function DisplayGame({ match: { params } }) {
  const gameRef = useFirestore().collection("Games").doc(params.id);
  const game = useFirestoreDocData(gameRef);

  if (!game.id) {
    throw new Error("Game doesn't exist");
  }

  return game.started ? (
    <GroupGameView game={game} />
  ) : (
    <PlayersJoining game={game} />
  );
}
