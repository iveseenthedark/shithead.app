import React from "react";
import { Box } from "@chakra-ui/core";

export const CardPlaceholder = ({ height = 14, border = 0.5, ...props }) => (
  <Box
    width={`${height * 0.71}rem`}
    height={`${height}rem`}
    border={`${border}rem dashed grey`}
    borderRadius="5px"
    {...props}
  />
);
