import React from "react";
import { Box, Button, Heading } from "@chakra-ui/core";
import { useFirestore, useFirestoreDocData } from "reactfire";
import { PlayerAvatar } from "../common/PlayerAvatar";
import API from "../api";
import { Play } from "./PlayView";

const Waiting = ({ game, player }) => {
  const doDeal = () => {
    API.startGame(game.id);
  };
  return (
    <>
      <PlayerAvatar player={player} />
      <Heading textAlign="center">Waiting for Game to Start</Heading>
      {player.dealer && (
        <Box padding={6}>
          <Button onClick={doDeal} size="lg" variantColor="teal">
            Deal
          </Button>
        </Box>
      )}
    </>
  );
};

export default function PlayGame({ match: { params } }) {
  const gameRef = useFirestore().collection("Games").doc(params.game);
  const game = useFirestoreDocData(gameRef);

  if (!game.id) {
    throw new Error("Game doesn't exist");
  }

  const player = game.players.find((player) => player.id === params.player);
  if (!player) {
    throw new Error("Unknown player");
  }

  return (
    <>
      {game.started ? (
        <Play game={game} player={player} />
      ) : (
        <Waiting game={game} player={player} />
      )}
    </>
  );
}
