import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input,
} from "@chakra-ui/core";
import API from "../api";

export default function StartGame({ match: { params } }) {
  const [name, setName] = useState();
  const [game, setGame] = useState();

  const history = useHistory();

  const onSubmit = (e) => {
    e.preventDefault();
    API.joinGame(game, name).then((player) => {
      history.push(`/play/${game}/${player.id}`);
    });
  };

  return (
    <>
      <Heading textAlign="center">Enter Game Code to Join</Heading>
      <form onSubmit={onSubmit}>
        <FormControl isRequired>
          <FormLabel htmlFor="game">Game Code</FormLabel>
          <Input
            type="game"
            id="game"
            defaultValue={game}
            onChange={(e) => setGame(e.target.value)}
          />
        </FormControl>
        <FormControl isRequired>
          <FormLabel htmlFor="name">Name</FormLabel>
          <Input
            type="name"
            id="name"
            onChange={(e) => setName(e.target.value)}
          />
        </FormControl>
        <Button mt={4} variantColor="teal" type="submit">
          Join
        </Button>
      </form>
    </>
  );
}
