export const CardTypes = {
  IN_HAND_CARD: "InHand",
  FACE_UP_CARD: "FaceUp",
  FACE_DN_CARD: "FaceDn",
};

export default {
  cardCode: (card) => {
    return `${card.value}${card.suit}`;
  },
};
