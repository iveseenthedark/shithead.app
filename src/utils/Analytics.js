import React, { useEffect } from "react";
import ReactGA from "react-ga";

ReactGA.initialize("UA-112615457-3");

/*
 <!-- Global site tag (gtag.js) - Google Analytics -->
 <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112615457-3"></script>
 <script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-112615457-3');
 </script>
 */
export const withTracker = (WrappedComponent, options = {}) => {
  const trackPage = (page) => {
    ReactGA.set({
      page,
      ...options,
    });
    ReactGA.pageview(page);
  };

  return (props) => {
    useEffect(() => trackPage(props.location.pathname), [
      props.location.pathname,
    ]);
    return <WrappedComponent {...props} />;
  };
};
