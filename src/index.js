import React from "react";
import ReactDOM from "react-dom";
import { CSSReset, ThemeProvider } from "@chakra-ui/core/dist";
import App from "./App";

ReactDOM.render(
  <ThemeProvider>
    <CSSReset />
    <App />
  </ThemeProvider>,
  document.getElementById("root")
);
